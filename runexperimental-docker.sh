#!/bin/bash -x

# Accepting following parameters:
#  $1: buildtype (Debug,Release)
#  $2: compiler version
#  $3: slotname

# Relying on following environment variables:
#  WORKSPACE (defined in by Docker run command )

# Directories needed:
#  - /ccache : To be used as CCACHE_DIR (mounted by Docker)
#  - /cvmfs : To access compilers, previous releases, ... (mounted by Docker)
#  - $WORKSPACE/lcgjenkins : LCGjenkins configuration scripts (mounted by Docker)
#  - $WORKSPACE/lcgcmake : LCGCMake rules to build packages (mounted by Docker)

THIS=$(dirname ${BASH_SOURCE[0]})
ARCH=$(uname -m)

# Check parameters
if [ $# -ge 3 ]; then
  BUILDTYPE=$1; shift
  COMPILER=$1; shift
  SLOTNAME=$1; shift
  LCG_VERSION=$SLOTNAME
else
  echo "$0: expecting 3 arguments: [buildtype] [compiler] [slotname]"
  return
fi

# Check environment variables
if [[ -z $WORKSPACE ]]; then
  return
fi

# avoid having permission problems
mkdir $WORKSPACE; cd $WORKSPACE

# Copy bind mounts to persist them
cp -r /lcg* $WORKSPACE/

# Working space and WORKSPACE variable have to equals
#if [[ $WORKSPACE != $PWD ]]; then
#  return
#fi

# Equal WORKDIR to WORKSPACE, some scripts use it instead of WORKSPACE
export WORKDIR=$WORKSPACE

# Allow scp/ssh connection to the jenkins server
sed -i 's/phsft/epsft/' ~/.ssh/config

weekday=`date +%a`
echo source $WORKSPACE/lcgjenkins/jk-setup.sh $BUILDTYPE $COMPILER ${LCG_VERSION} > setup.sh
source lcgjenkins/jk-setup.sh $BUILDTYPE $COMPILER ${LCG_VERSION}
lcgjenkins/isDone.sh 0

if [[ "$PLATFORM" = "*gcc*" ]]; then
## ccache support
echo source $WORKSPACE/lcgjenkins/ccache-setup.sh >> setup.sh
source $WORKSPACE/lcgjenkins/ccache-setup.sh

#ln -sf /ccache ~/.ccache

export CCACHE_BASEDIR=$WORKDIR
# just to chech if ccache is taking compilers from cvmfs
ccache gcc -v
# show ccache configuration
#ccache -p
fi
gcc -v

lcgjenkins/jk-runbuild.sh

cd $WORKSPACE
jsonfile="$SLOT-$PLATFORM.txt"
today=`date +%Y-%m-%d`
lcgjenkins/lcgsoft/ReleaseSummaryReader $SLOT $PLATFORM nightlies
lcgjenkins/lcgsoft/fill_release.py -f $WORKSPACE/$jsonfile -d "$today" -e "Daily publication of the $SLOT nightliy build" -o "NO"

if [ "x$COPY_LOGS" = "xtrue" ]; then
    rm -rf /lcgcmake/logs
    mkdir /lcgcmake/logs
    find $WORKSPACE/build/{generators,externals,pyexternals,projects} -maxdepth 4 -iname "*.log" -exec cp {} /lcgcmake/logs/ \;
fi

$WORKSPACE/lcgcmake/cmake/scripts/create_lcg_view.py -l $WORKSPACE/install -p $PLATFORM -d -B $WORKSPACE/views

export BUILDHOSTNAME=`hostname`
if [[ ! "$PATTERN" ]]; then
    PATTERN=".*"
fi

if [[ "$RUN_TEST" ]]; then
    cd $WORKSPACE
    cp $WORKSPACE/build/Testing/TAG /lcgtest
    cp $WORKSPACE/lcgjenkins/macros.cmake $WORKSPACE/lcgtest
    if [[ ! -e "$WORKSPACE/lcgtest/runtest-docker.sh" ]]; then
        cp -r /lcgtest $WORKSPACE/
    fi
    bash -x $WORKSPACE/lcgtest/runtest-docker.sh "$PATTERN" "$VIEW"
fi

export PLATFORM=`$WORKSPACE/lcgjenkins/getPlatform.py`
cat > $PROPERTIES_PATH/properties.txt << EOF
PLATFORM=${PLATFORM}
COMPILER=${COMPILER}
weekday=${weekday}
EOF


