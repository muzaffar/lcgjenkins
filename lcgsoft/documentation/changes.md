
# Web changes

## Migrations

### Database migration
The database has been migrated from a local SQLite instance to a PostgreSQL hosted by the IT department. This database has daily automatic backups.

At the moment it is neccesary to connect by using a password, but we are working on allowing specific users direct connection using SSL.

### Hosting static files with Apache server
An Apache server (httpd) is being used to serve the application in the production environment. It automatically manages the Django application execution and serves its static files.

## Performance improvements

### Caching
A memory caching system has been setup by using [memcached](https://memcached.org/). When a page has been cached its load time is exptreamly reduced. For example, the main page goes from 1.5 seconds when uncached to 0.1 when cached.

### Optimized database accesses
Some manual SQL queries off the application where called multiple times during a single rendering. Now, or it is only called once, or its result has been cached.

The best example for this is the comparisons view. The query time for it has been extremely reduced, as multiple SQL queries where condensed into a single one. The calculation time of this page was reduced from 2 seconds (static html) to 200 ms

### Debug mode Off
The Debug mode was enabled by default in the webpage. Appart of the probable performance enhances, if debug is enabled a temporal server for static files is set up. This is not recommended as it may be not secure enough.

## UI/UX improvements

### Minimization and SASS
Using a library called _django-compressor_, the javascript code used by the application is being minified in order to use as less space as possible.

Apart from that, it also compiles [SCSS](http://sass-lang.com/), which is a more powerfull syntax for CSS.

### Client side code (Javascript)
A library called [_tablesorter_](http://tablesorter.com/docs/) is being used in order to perform table sorting at the client side.

### CSS rework
The CSS Framework [_Bootstrap_](http://getbootstrap.com/) is being used.

# Scripts

## generate_changelog

This script generates a changelog between 2 different releases. This two can be passed as parameters of the script or not. If there are no parameters, the last two versions will be taken into account.
