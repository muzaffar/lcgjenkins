# LCGSOFT Webpage Documentation

## Intallation

### Dependencies
You need to install the following dependencies:
```bash
sudo pip install Django==1.6.11 django-compressor==1.6 django-libsass south psycopg2 pyfunctional python-memcached markdown
```

```bash
# If you have apt-get
sudo apt-get install memcached postgresql

# If you have yum
sudo yum install memcached postgresql
```

#### Dependencies description
* **django:** our web framework choice. We are using version 1.6.11 at the moment, which is the one provided in the IT repositories
* **django-compressor:** compresses the css and js code in production settings. Versions newer than 1.6 crash with Django 1.6.11
* **django-libsass:** used for compiling SASS and SCSS
* **south:** used for migrating the database
* **psycopg2:** used for connection to the PostgreSQL database
* **pyfunctional:** improves python's functional operations
* **python-memcached:** needed to connect to memcached backend
* **markdown:** used for compiling markdown into html

### Production dependencies
The dependencies server has two install 2 more things:
```bash
yum install httpd mod_wsgi
```
#### Dependencies description
* **httpd:** Apache server
* **mod_wsgi:** allows Apache to run the Django website

### Run the server

In order to run the server with debug settings use:
```bash
python manage.py runserver
```

## Database

The database is a PostgreSQL version 9.6 hosted by the IT services at CERN. It has an automatic daily backup. It also has the possibility to make tape backups, altough they are not needed for our application.

### Database connection
In order to manually connect to the database type:
```bash
psql -h dbod-lcgsoft.cern.ch -p 6620 -U admin djangodb
```
After this, you will be requested the database password.

## Servers configuration

Both servers have the following packages puppetized:

```
python
python-django
python-pip
python-psycopg2
python-django-south
python-django-compressor
python-markdown
python-memcached
```

But the following aren't provided in the CERN repository, so they should be manually installed with pip.

```bash
pip install pyfunctional libsass
```

### Webpage production server

The application is located at the server **lcgapp-centos7-lcgsoft** in path **/var/www/lcgjenkins/lcgsoft/webpage**.

In order for the server to work correctly you have to:
* Disable selinux, in order to avoid issues with the webhook
* Ensure that **/var/www/lcgjenkins** is owned by the apache user.

#### Apache server configuration
This is the content of the file **/etc/httpd/conf.d/lcgsoft.conf**:

```
<VirtualHost *:80>
    Alias /static /var/www/lcgjenkins/lcgsoft/webpage/data/static
    <Directory /var/www/lcgjenkins/lcgsoft/webpage/data/static>
        Require all granted
    </Directory>

    <Directory /var/www/lcgjenkins/lcgsoft/webpage/relinfo>
        <Files wsgi.py>
            Require all granted
        </Files>
    </Directory>

    WSGIDaemonProcess lcgsoft python-path=/var/www/lcgjenkins/lcgsoft/webpage
    WSGIProcessGroup lcgsoft
    WSGIScriptAlias / /var/www/lcgjenkins/lcgsoft/webpage/relinfo/wsgi.py

    ScriptAlias /webhook /var/www/lcgjenkins/lcgsoft/webpage/webhook/regenerate_web.sh
    <Location "/webhook">
        # Gitlab server address
        Require ip 188.184.64
    </Location>

    RewriteEngine on
    RewriteCond %{REQUEST_METHOD} ^(TRACE|TRACK)
    RewriteRule .* - [F]
</VirtualHost>
```

It sets up the connection to the django application and disables HTTP methods TRACK and TRACE, I requested by the IT security department.

#### Reload static files changes and flush memory

After any changes in the code, you will have to reload the services:

```bash
# With sudo rights
sh lcgsoft/regenerate_web.sh
```

## Modify application model

It is possible to modify the model of the application, in order to add more columns to the database.

### Modify models.py file

First of all you'll have to edit the file **models.py**, found inside **relinfo** folder. You will look for the specific model to modify and add/remove information.

### Migrate database

Now, using south library, you will make the database reflect the changes made into the model. You just have to run:

```bash
# Detect the models changes and generate the migration
python manage.py schemamigration relinfo --auto

# Perform the migration
python manage.py migrate relinfo
```

After this, both the model and the database should contain the same information.

### Jenkins node serverd

The server is **lcgapp-centos7-jenkinslcgsoft**.

## Jenkins workflow

At the moment there are 2 different jobs used for nightly release,  [lcg_nightlies_dbpublish_dev3](https://epsft-jenkins.cern.ch/job/lcg_nightlies_dbpublish_dev3/) and [lcg_nightlies_dbpublish_dev4](https://epsft-jenkins.cern.ch/job/lcg_nightlies_dbpublish_dev4/). This 2 jobs run automatically after each daily compilation.

There is also a job for manual releases [lcg_releases_dbpublish](https://epsft-jenkins.cern.ch/job/lcg_releases_dbpublish/).

### LCGCMake clone/update
The first step is cloning and updating lcgcmake repository.

### Update database from packages.json file

The file packages.json is parsed and its contents are compared to the data in the DB. If any change is detected, the DB is updated.

### Read compilation files and update DB

The files containing the compilation info is parsed and a new json file is generated. This one is used to update the DB and then deleted.

### Generate release notes (if release)

If the current execution is marked as release. A release diff text will be generated comparing the new version with the second newest (that are not python3).

![Image](./imgs/release_notes.png)

### Refresh packages.json file

A new packages.json file is generated and uploaded into a git repository (if there is any change).

## Application folder structure
```
.
|-- data
|   `-- static  # Static files serverd by the app
|-- db_settings_template.py  # DB configuration template
|-- __init__.py
|-- manage.py
|-- no_debug_settings.py  # Production only settings
|-- regenerate_web.sh  # Script that flushes cache and reloads static files
|-- relinfo  # Main src folder
|   |-- migrations # History about database migrations (automatically generated when editing the model as explained above)
|   |-- models.py  # Application models
|   |-- templatetags
|   |   |-- extras.py  # Extra methods for templates
|   |-- tests.py
|   |-- views.py  # Views of the application
|   `-- wsgi.py  # Allos httpd to run the webpage
|-- settings.py  # Settings file
|-- static
|   |-- jquery.tablesorter.min.js  # Client-side table sorter
|   |-- style.scss  # SCSS style
|   |-- tables.js  # Script for main view
|-- templates
|   |-- base.html  # Parent of the other views
|   `-- relinfo
|       |-- compare.html  # Comparison of 2 releases
|       |-- main.html  # Main view
|       |-- package.html  # View of an specific package
|       |-- package_version.html  # View of an specific package's tag
|       |-- release.html  # View of an specific release
│       `-- release_notes.html # Shows the release notes of a specific release
|
|-- urls.py  # Map between urls and views
`-- webhook
        `-- regenerate_web.sh # Webhook for refreshing the page

```

## Scripts

The application scripts are located at the path lcgjenkins/lcgsoft.

This data shown here is the result of executing the script with -h flag, which shows help.

### How to run scripts locally

In order to run scripts locally you will have to do the following:

```bash
source setup.sh DB_ADMIN_PASSWORD # Found inside lcgsoft folder
```

This will set up all needed enviromental variables.

### release_summary_reader
```
usage: release_summary_reader.py [-h] lcg_version platform buildtype

Read the compilation of a given release and platform a generate a json file
that will be uploaded to a database

positional arguments:
  lcg_version  Name of the release
  platform     Platform of the buield
  buildtype    Build type, release or debug

optional arguments:
  -h, --help   show this help message and exit
```
### fill_release
```
usage: fill_release.py [-h] [-d DATE_INPUT] [-e DESCRIPTION_INPUT]
                       [-o REMOVE_OPTION] [--out OUT]
                       filename

Reads the json file generated by release_summary_reader and uploads it to the
db. It also generates a json file that can be used to update the packages
information.

positional arguments:
  filename              json file name

optional arguments:
  -h, --help            show this help message and exit
  -d DATE_INPUT         date of release YYYY-MM-DD
  -e DESCRIPTION_INPUT  brief explanation of the release
  -o REMOVE_OPTION      Remove option YES/NO
  --out OUT             Output file for the packages file
```
### generate_changelog
```
usage: generate_changelog.py [-h] [--out OUT] [old_version] [new_version]

Generates a changelog file between two different releases.

positional arguments:
  old_version  Old release
  new_version  New release

optional arguments:
  -h, --help   show this help message and exit
  --out OUT    Output file
```
### update_from_file
```
usage: update_from_file.py [-h] path

Parses a json file containing information about the packages. If there is any
difference with the DB, the DB is updated.

positional arguments:
  path        Path of the JSON file with the package info

optional arguments:
  -h, --help  show this help message and exit
```

### clear_release_platform
```
usage: clear_release_platform.py [-h] release platforms [platforms ...]

Removes the selected platforms from a given release.

positional arguments:
  release     The name of the release
  platforms   The platforms to be deleted

optional arguments:
  -h, --help  show this help message and exit
```

### release_manager
```
usage: release_manager.py [-h] [-p] [-d DESCRIPTION]
                          [-rnin RELEASE_NOTES_FROM_FILE]
                          [-rnout RELEASE_NOTES_TO_FILE] [--delete]
                          version

Script to simplify the modification of releases' data

positional arguments:
  version               Release version name

optional arguments:
  -h, --help            show this help message and exit
  -p, --pretty          Prints data in a pretty way
  -d DESCRIPTION, --description DESCRIPTION
                        Set the release description
  -rnin RELEASE_NOTES_FROM_FILE, --release-notes-from-file RELEASE_NOTES_FROM_FILE
                        Sets the release notes from a given file
  -rnout RELEASE_NOTES_TO_FILE, --release-notes-to-file RELEASE_NOTES_TO_FILE
                        Writes the release notes in a file
  --delete              Deletes de selected release and all its data
```

### package_manager
```
usage: package_manager.py [-h] [-p] [-d DESCRIPTION] [--home-page HOME_PAGE]
                          [--full-name FULL_NAME] [-c CATEGORY] [-t TYPE]
                          [-l LANGUAGE] [--delete]
                          name

Script to simplify the modification of packages' data

positional arguments:
  name                  Package name

optional arguments:
  -h, --help            show this help message and exit
  -p, --pretty          Prints data in a pretty way
  -d DESCRIPTION, --description DESCRIPTION
                        Set the package description
  --home-page HOME_PAGE
                        Set the home page of the package
  --full-name FULL_NAME
                        Set the full name of the package
  -c CATEGORY, --category CATEGORY
                        Set the category of the package
  -t TYPE, --type TYPE  Set the type of the package
  -l LANGUAGE, --language LANGUAGE
                        Set the language of the package
  --delete              Deletes de selected package and all its data
```

## Bugs
### django-compressor
The latest version we can use of django-compressor is 1.6. In this version there is currently a bug that affects the compression of parametrized strings:

```js
`Example: ${i}`
```
In some, cases, the space after a parmeter is removed, which can cause errors, such as in this case:

```js
`#paginator li:nth-child(${i+2}) a`
```

The previous string is compressed as:

```js
`#paginator li:nth-child(${i+2})a`
```

Which becomes an invalid jquery query.
