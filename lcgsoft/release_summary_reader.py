#!/usr/bin/env python

import datetime
import os
import sys
from string import Template
from datetime import date
import calendar
import json
import argparse


def extract_package_info(entry):
    """Extract the package information of a line formed like this:
      xqilla; cefdd;  2.2.4p1; ./xqilla/2.2.4p1/x86_64-slc6-gcc48-dbg; XercesC-8ccd5
     """
    package = {}
    name, hash, version, directory, dependency_string = entry.split(";")
    package['name'] = name.strip()
    package['hash'] = hash.strip()
    package['version'] = version.strip()

    if "MCGenerators" in directory or "Grid" in directory:
        dest_name = directory.split("/")[2]
        destination_directory = directory.split('/', 3)
        dest_directory = destination_directory[1] + \
            "/" + destination_directory[2]
    else:
        dest_name = directory.split("/")[1]
        destination_directory = directory.split('/', 2)
        dest_directory = destination_directory[1]

    package['dest_name'] = dest_name
    package['dest_directory'] = dest_directory

    dependencies = [f.lstrip()
                    for f in dependency_string.split(",") if f.lstrip() != ""]
    package["dependencies"] = dependencies
    return "%s-%s" % (package['name'], package['hash']), package


def num_there(s):
    return any(i.isdigit() for i in s)


def extract_package_info_from_buildinfo(filename):
    filecontent = file(filename).read()
    components = filecontent.split(",")

    offset = 3
    if "GITHASH" in filecontent and "DEPENDS" not in filecontent and "DIRECTORY" in filecontent:
        case = 1

    elif "GITHASH" in filecontent and "DEPENDS" not in filecontent and "DIRECTORY" not in filecontent:
        case = 2
    elif "SVNREVISION" in filecontent:
        case = 2
    elif "DEPENDS" in filecontent:
        case = 3
    package = {}

    package['hash'] = components[offset].split()[1]
    package['dest_name'] = components[offset + 1].split()[1]

    if "MCGenerators" in filename:
        package['dest_directory'] = "MCGenerators" + \
            "/" + components[offset + 1].split()[1]
    elif "Grid" in filename:
        package['dest_directory'] = "Grid" + \
            "/" + components[offset + 1].split()[1]
    else:
        package['dest_directory'] = components[offset + 1].split()[1]

    if (case == 1):
        package['name'] = components[offset + 3].split()[1]
        package['version'] = components[offset + 4].split()[1]

        l = components[offset + 5:-1]
        l = [num.rsplit("-", 5) for num in l]
        ll = [num[0] + "-" + num[-1] for num in l]
        package["dependencies"] = ll

    elif(case == 2):
        package['name'] = components[offset + 2].split()[1]
        package['version'] = components[offset + 3].split()[1]
        package["dependencies"] = components[offset + 4:-1]

    elif(case == 3):
        package['name'] = components[offset + 3].split()[1]
        package['version'] = components[offset + 4].split()[1]
        l = components[offset + 6:-1]
        l = [i.replace('DEPENDS:', "") for i in l]
        l = [num.strip() for num in l]
        l = [num.rsplit("-", 5) for num in l]
        ll = [num[0] + "-" + num[-1] for num in l]
        package["dependencies"] = ll

    return "%s-%s" % (package['name'], package['hash']), package


def parse_summary_file(lcg_version, platform):
    distributions = ["externals", "generators"]

    base = ' '

    packages_all = []
    for i in distributions:

        filename = os.path.join(base, "LCG_%s" %
                                lcg_version, "LCG_%s_%s.txt" % (i, platform))
        filecontent = file(filename).read()

        release = {}
        packages = {}
        release["description"] = {"version": lcg_version, "platform": platform}
        iterator = iter(filecontent.splitlines())
        iterator.next()  # skip version line
        iterator.next()  # skip platform line
        iterator.next()  # skip compiler line VALID ONLY FROM 76root6
        for line in iterator:
            name_hash, package = extract_package_info(line)
            packages[name_hash] = package
            all_packages = packages.copy()
            all_packages.update(packages)

    return all_packages


def build_releaseinfo_from_installarea(lcg_version, platform, releasedir, buildtype):
    packages_dummy = {}
    if "dummy" in platform:
        return packages_dummy

    releasedepth = releasedir.count("/")

    packages = {}

    # parse externals files at /cvmfs/sft.cern.ch/lcg/nightlies/dev3/
    generators_file = releasedir + 'LCG_generators_' + platform + '.txt'
    externals_file = releasedir + 'LCG_externals_' + platform + '.txt'

    print 'Generators file:', generators_file
    print 'Externals file:', externals_file

    if not os.path.exists(generators_file) or not os.path.exists(externals_file):
        return None
    else:
        readBuildFile(file(generators_file), packages)
        readBuildFile(file(externals_file), packages)

    return packages


def readBuildFile(f, packages={}):
    # Skip 3 first lines
    f.readline()
    f.readline()
    f.readline()

    for line in f:
        parts = line.strip().split(';')
        package_name = parts[0].strip()
        package_hash = parts[1].strip()
        package_version = parts[2].strip()
        package_dependencies = []
        if len(parts) >= 5:
            package_dependencies = parts[4].split(',')
        packages[package_name + '-' + package_hash] = {
            'name': package_name,
            'hash': package_hash,
            'version': package_version,
            'dependencies': package_dependencies
        }


def getHeader(lcg_version, platform, conts):
    return json.dumps({
        'description': {
            'version': lcg_version,
            'platform': platform
        },
        'packages': conts
    }, indent=4)


##########################
if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description='Read the compilation of a given release and platform a generate a json file that will be uploaded to a database')
    parser.add_argument('lcg_version', help='Name of the release')
    parser.add_argument('platform', help='Platform of the buield')
    parser.add_argument('buildtype', help='Build type, release or debug')

    args = parser.parse_args()

    my_date = date.today()
    today = calendar.day_name[my_date.weekday()][0:3]

    if "release" in args.buildtype:
        base = "/cvmfs/sft.cern.ch/lcg/releases/LCG_%s/" % (args.lcg_version)
    else:
        base = "/cvmfs/sft.cern.ch/lcg/nightlies/%s/%s/" % (
            args.lcg_version, today)

    print 'Path:', base

    conts = build_releaseinfo_from_installarea(
        args.lcg_version, args.platform, base, args.buildtype)
    if conts:
        file_contains = getHeader(args.lcg_version, args.platform, conts)
        file_name = "%s-%s.%s" % (args.lcg_version, args.platform, "json")

        f = open(file_name, "w")
        f.write(file_contains)
        f.close()
    else:
        print 'Compilation not valid'
