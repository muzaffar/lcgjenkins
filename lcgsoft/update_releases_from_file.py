#!/usr/bin/env python

import argparse
import json
import os
os.environ['DJANGO_SETTINGS_MODULE'] = 'settings'
from relinfo.models import Release
from functional import seq


def check_release(release):
    release_db = Release.objects.get(version=release['version'])

    if release_db.extra_notes != release['extra_notes']:
        release_db.extra_notes = release['extra_notes']
        release_db.save()


if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        description='Parses a json file containing information about the releases. If there is any difference with the DB, the DB is updated.')

    parser.add_argument(
        'path', help='Path of the JSON file with the releases info')

    args = parser.parse_args()

    releases = json.load(open(args.path, 'r'))

    for release in releases:
        check_release(release)
