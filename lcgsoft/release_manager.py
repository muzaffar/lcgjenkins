import os
import argparse
os.environ['DJANGO_SETTINGS_MODULE'] = 'settings'
from relinfo.models import Release
import json


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Script to simplify the modification of releases\' data')

    parser.add_argument('version', help='Release version name')
    parser.add_argument('-p', '--pretty', help='Prints data in a pretty way', action='store_true')

    parser.add_argument('-d', '--description', help='Set the release description')
    parser.add_argument('-rnin', '--release-notes-from-file', help='Sets the release notes from a given file')
    parser.add_argument('-rnout', '--release-notes-to-file', help="Writes the release notes in a file")

    parser.add_argument('--delete', help="Deletes de selected release and all its data", action='store_true')

    args = parser.parse_args()

    try:
        release = Release.objects.get(version=args.version)

        release_dict = {
            'version': release.version,
            'description': release.description,
            'release_notes': release.release_notes
        }
        if args.pretty:
            print json.dumps(release_dict, indent=4, sort_keys=True)
        else:
            print release_dict

        if args.release_notes_to_file:
            with open(args.release_notes_to_file, 'w') as f:
                f.write(release.release_notes)

            print 'File exported successfully'

        if args.description:
            release.description = args.description
            release.save()

        if args.release_notes_from_file:
            with open(args.release_notes_to_file, 'r') as f:
                release.release_notes = f.read()
                release.save()

        if args.delete:
            release.delete()
            print 'Deleted successfully'

    except:
        print 'Release not found'
