#!/usr/bin/env sh

cd $(dirname "$0")/..

git pull origin >> webhook.log

python manage.py collectstatic --noinput --clear >> webhook.log
python manage.py compress --settings=no_debug_settings >> webhook.log

touch relinfo/wsgi.py

echo 'flush_all' | >&2 nc localhost 11211  >> webhook.log

echo 'Success' >> webhook.log

echo "Content-Type: text/html"
echo
echo
echo "OK"
