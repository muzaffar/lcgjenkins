from django.conf.urls import patterns, include, url
from django.conf import settings
from django.conf.urls.static import static

urlpatterns = patterns('',
    url(r'^$', 'relinfo.views.allreleases'),
    url(r'^release/(?P<version>\w+)/$', 'relinfo.views.release'),
    url(r'^pkg/(?P<package_name>[a-zA-Z0-9_.-/+/-]+)/$', 'relinfo.views.package'),
    url(r'^pkgver/(?P<package_name>[a-zA-Z0-9_.-/+/-]+)/(?:(?P<tag_name>[a-zA-Z0-9_.-/+/-]+))?/$', 'relinfo.views.tag'),
    url(r'^pkgrel/(?P<package_name>[a-zA-Z0-9_.-/+/-]+)/(?P<release_id>\w+)/$', 'relinfo.views.packagerelease'),
    url(r'^compare/(?P<version1>\w+)/(?P<version2>\w+)$', 'relinfo.views.compare'),
    url(r'^api/packages$', 'relinfo.views.packageList'),
    url(r'^release_notes/(?P<release_version>\w+)/$', 'relinfo.views.release_notes'),
    url(r'^webhook$', 'relinfo.views.webhook'),
) + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
