from django import template
from functional import seq
from relinfo.models import ReleaseTag
import markdown

register = template.Library()


@register.filter
def arrayAccess(value, index):
    return value[index]


@register.filter
def getDistinctReleasesForTag(tag):
    return tag.release_set.order_by("-version").distinct("version").all()


@register.filter
def getDependenciesForReleaseAndTag(value, arg):
    from django.db import connection, transaction
    cursor = connection.cursor()
    cursor.execute('''SELECT DISTINCT p.name, t.name
                      FROM relinfo_tag t
                      INNER JOIN relinfo_package p ON t.package_id = p.id
                      INNER JOIN relinfo_tagdependency td ON t.id = td.to_tag_id
                      INNER JOIN relinfo_releasetag rt ON td.to_tag_id = rt.tag_id
                      INNER JOIN relinfo_release r ON r.id = rt.release_id
                      WHERE td.from_tag_id = 776 AND r.version = 'dev4'
                      ORDER BY t.name;
                   ''', (arg, value))
    row = cursor.fetchall()

    return row

@register.filter(name='split')
def split(value, arg):
    return value.split(arg)

@register.filter
def markdownify(text):
    # safe_mode governs how the function handles raw HTML
    return markdown.markdown(text, safe_mode='escape')
