# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding field 'Release.release_notes'
        db.add_column(u'relinfo_release', 'release_notes',
                      self.gf('django.db.models.fields.TextField')(null=True, blank=True),
                      keep_default=False)


    def backwards(self, orm):
        # Deleting field 'Release.release_notes'
        db.delete_column(u'relinfo_release', 'release_notes')


    models = {
        u'relinfo.architecture': {
            'Meta': {'object_name': 'Architecture'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '60'}),
            'parent': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['relinfo.Architecture']", 'null': 'True', 'blank': 'True'})
        },
        u'relinfo.compiler': {
            'Meta': {'object_name': 'Compiler'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '60'}),
            'parent': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['relinfo.Compiler']", 'null': 'True', 'blank': 'True'})
        },
        u'relinfo.contact': {
            'Meta': {'object_name': 'Contact'},
            'email': ('django.db.models.fields.EmailField', [], {'unique': 'True', 'max_length': '75'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '150', 'null': 'True', 'blank': 'True'})
        },
        u'relinfo.license': {
            'Meta': {'object_name': 'License'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '128'}),
            'url': ('django.db.models.fields.URLField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'})
        },
        u'relinfo.mode': {
            'Meta': {'object_name': 'Mode'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '60'})
        },
        u'relinfo.osystem': {
            'Meta': {'object_name': 'Osystem'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '60'}),
            'parent': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['relinfo.Osystem']", 'null': 'True', 'blank': 'True'})
        },
        u'relinfo.package': {
            'Meta': {'object_name': 'Package'},
            'category': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'contacts': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['relinfo.Contact']", 'through': u"orm['relinfo.PackageContact']", 'symmetrical': 'False'}),
            'description': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'fullname': ('django.db.models.fields.CharField', [], {'max_length': '135'}),
            'homepage': ('django.db.models.fields.URLField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'language': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'license': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['relinfo.License']", 'null': 'True', 'blank': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '135'}),
            'type': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'})
        },
        u'relinfo.packagecontact': {
            'Meta': {'unique_together': "(('package', 'contact'),)", 'object_name': 'PackageContact'},
            'contact': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['relinfo.Contact']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'package': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['relinfo.Package']"})
        },
        u'relinfo.platform': {
            'Meta': {'object_name': 'Platform'},
            'architecture': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['relinfo.Architecture']", 'null': 'True', 'blank': 'True'}),
            'compiler': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['relinfo.Compiler']", 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_abstract': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'mode': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['relinfo.Mode']", 'null': 'True', 'blank': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '135'}),
            'osystem': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['relinfo.Osystem']", 'null': 'True', 'blank': 'True'})
        },
        u'relinfo.release': {
            'Meta': {'object_name': 'Release'},
            'cancelled': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            'date': ('django.db.models.fields.DateTimeField', [], {}),
            'description': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'packages': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['relinfo.Package']", 'through': u"orm['relinfo.ReleasePackage']", 'symmetrical': 'False'}),
            'platforms': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['relinfo.Platform']", 'through': u"orm['relinfo.ReleasePlatform']", 'symmetrical': 'False'}),
            'release_notes': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'tags': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['relinfo.Tag']", 'through': u"orm['relinfo.ReleaseTag']", 'symmetrical': 'False'}),
            'type': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'version': ('django.db.models.fields.CharField', [], {'max_length': '30'})
        },
        u'relinfo.releasepackage': {
            'Meta': {'object_name': 'ReleasePackage'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'package': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['relinfo.Package']"}),
            'platform': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['relinfo.Platform']", 'null': 'True', 'blank': 'True'}),
            'release': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['relinfo.Release']"})
        },
        u'relinfo.releaseplatform': {
            'Meta': {'object_name': 'ReleasePlatform'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'platform': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['relinfo.Platform']"}),
            'release': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['relinfo.Release']"})
        },
        u'relinfo.releasetag': {
            'Meta': {'object_name': 'ReleaseTag'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'platform': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['relinfo.Platform']", 'null': 'True', 'blank': 'True'}),
            'release': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['relinfo.Release']"}),
            'tag': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['relinfo.Tag']"})
        },
        u'relinfo.tag': {
            'Meta': {'object_name': 'Tag'},
            'dependencies': ('django.db.models.fields.related.ManyToManyField', [], {'related_name': "'users'", 'symmetrical': 'False', 'through': u"orm['relinfo.TagDependency']", 'to': u"orm['relinfo.Tag']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'package': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['relinfo.Package']"})
        },
        u'relinfo.tagdependency': {
            'Meta': {'object_name': 'TagDependency'},
            'from_tag': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'from_tags'", 'to': u"orm['relinfo.Tag']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'platform': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['relinfo.Platform']", 'null': 'True', 'blank': 'True'}),
            'to_tag': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'to_tags'", 'to': u"orm['relinfo.Tag']"})
        }
    }

    complete_apps = ['relinfo']