#!/usr/bin/env bash
shopt -s extglob

VERSION=$1
wget -P nxcals/ http://artifactory.cern.ch/ds-release-local/cern/nxcals/nxcals-data-access/${VERSION}/nxcals-data-access-${VERSION}.jar
wget -P nxcals/ http://artifactory.cern.ch/ds-release-local/cern/nxcals/nxcals-data-access/${VERSION}/nxcals-data-access-${VERSION}.pom

mkdir -p sources
tar -czvf sources/accsoft-nxcals-data-access-${VERSION}.tar.gz nxcals
pushd nxcals
rm !(settings.xml)
popd

git clone https://gitlab.cern.ch/acc-logging-team/nxcals.git nxcals-repo
pushd nxcals-repo
git checkout RELEASE-${VERSION}
tar -czvf ../sources/accsoft-nxcals-data-access-python-${VERSION}.tar.gz data-access-python3
popd

rm -rf nxcals-repo

