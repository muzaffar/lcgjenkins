echo "The following sources will be installed:"
ls -1 sources

read -p "Should we procceed? [y/n]" -n 1 -r
echo
if [[ $REPLY =~ ^[Yy]$ ]]
then
    scp sources/* sftnight@lcgapp-slc6-physical2:/eos/project/l/lcg/www/lcgpackages/tarFiles/sources/
    
    read -p "Should we delete the sources? [y/n]" -n 1 -r
    echo
    if [[ $REPLY =~ ^[Yy]$ ]]
    then
        rm -rf sources
    fi
fi

