#!/bin/bash
echo "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX"
echo "Copying clang built in $ORIGIN_NODE"
echo "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX"

export PLATFORM=$1
export LLVMREV=$2

for iterations in {1..10}
do
  cvmfs_server transaction sft.cern.ch

  if [ "$?" == "1" ]; then
    if  [[ "$iterations" == "10" ]]; then
      echo "Too many tries... exiting"
      exit 1
    else
       echo "Transaction is already open. Going to sleep..."
       sleep 10m
    fi
  else
    break
  fi
done

basewrite=/cvmfs/sft.cern.ch/lcg/contrib/llvm/

clangbuilds="${basewrite}builds/${PLATFORM}"
latestclangbuild="${basewrite}latest/${PLATFORM}"

readfrom="/var/spool/cvmfs/sft.cern.ch/sftnight/clang_builds/clang_${LLVMREV}_${PLATFORM}"

exitStatus=0

mkdir -p "$clangbuilds"
exitStatus=$(( $exitStatus + $? ))
#mkdir -p "${latestclangbuild}"
#exitStatus=$(( $exitStatus + $? ))

# Get number of installed builds on afs
numdir=`find ${clangbuilds}/* -maxdepth 0 -type d | wc -l`

while [[ $numdir -ge 5 ]]; do
	# Look for the oldest clang build
	read -r -d $'\0' line < <(find ${clangbuilds} -maxdepth 1 -type d -printf '%T@ %p\0' 2>/dev/null | sort -z -n)
	oldest="${line#* }"

	# Remove it
    echo "Removing oldest build: $oldest..."
	rm -rf $oldest
    exitStatus=$(( $exitStatus + $? ))
    numdir=`find ${clangbuilds}/* -maxdepth 0 -type d | wc -l`
done

# Copy the newest one from the remote node
cp -r "$readfrom" "$clangbuilds"
exitStatus=$(( $exitStatus + $? ))
mv "$clangbuilds/clang_${LLVMREV}_${PLATFORM}" "$clangbuilds/clang_build_${LLVMREV}"
exitStatus=$(( $exitStatus + $? ))

# Remake the link to the latest one
ln -sfn $clangbuilds/clang_build_$LLVMREV $latestclangbuild
exitStatus=$(( $exitStatus + $? ))

if [ "$exitStatus" == "0" ]; then
	echo "The installation of the last clang build has worked"
	cd $HOME
	cvmfs_server publish sft.cern.ch
else
	echo "The installation of the last clang build has not worked. We exit here"
	cd $HOME
	cvmfs_server abort -f sft.cern.ch
	exit 1
fi
