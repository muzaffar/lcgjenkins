#!/bin/bash
if [ $# -lt 2 ]; then
  echo Usage: install_release_from_rpm.sh RELEASE PLATFORM [REVISION]
  exit 0
fi

RELEASE=$1; shift;
PLATFORM=$1; shift;
if [ -z $1 ]; then
  REVISION=$RELEASE
else
  REVISION=$1; shift
fi

export MYSITEROOT=$(pwd)
LCGRPM/install/lcg_install.sh install LCG_${RELEASE}_${PLATFORM}-1.0.0-${RELEASE}
LCGRPM/install/lcg_install.sh install LCG_generators_${RELEASE}_${PLATFORM}-1.0.0-${REVISION}
