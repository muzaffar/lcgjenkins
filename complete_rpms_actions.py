#!/usr/bin/env python
import os
import sys
from subprocess import Popen

from common_parameters import tmparea, officialarea


def copy_to_eos(from_, to):
    def execute_cmd(cmd):
        p = Popen(cmd, shell=True)
        p.wait()
        return p.returncode, p.stdout, p.stderr

    command = "xrdcp %s %s" % (from_, to)
    print "Copy", from_, "to", to,
    p = execute_cmd(command)
    if p[0] == 0:
        print "\t[OK]"
    elif p[0] == 54:
        print "\t[EXISTS]"
    else:
        print "\t[ERROR]"
        print p[1]
        print p[2]


parameters = sys.argv
version = parameters[1]

lcg_subdirectory = "LCG_" + version + "release"
lcg_directory = officialarea + lcg_subdirectory
os.environ['EOS_MGM_URL'] = "root://eosuser.cern.ch"
list_reference = []
list_official = []
common_rpms = []
list_to_copy = []
full_rmps = []

if not os.path.exists(lcg_directory):
    os.makedirs(lcg_directory)

os.chdir(officialarea)
for j in os.listdir('.'):
    if "LCG_" not in j:
        rpm_prefix = j.split("-1.0.0")[0]
        list_official.append(rpm_prefix)

os.chdir(tmparea)
for f in os.listdir('.'):
    if "LCG_" not in f:
        rpm_prefix = f.split("-1.0.0")[0]
        list_reference.append(rpm_prefix)

common_rpms = set(list_reference).intersection(list_official)

for item in list_reference:
    if item not in common_rpms:
        list_to_copy.append(item)
    else:
        pass

target = "root://eosuser.cern.ch/%s/%s" % (officialarea, lcg_subdirectory)

for f in os.listdir('.'):
    if "LCG_" in f:
        if version in f:
            copy_to_eos(f, target)

target = "root://eosuser.cern.ch/%s" % (officialarea,)

for t in list_to_copy:
    copy_to_eos(t + '*', target)
