#!/bin/bash

# This script downloads and builds the clang compiler 
# Prerequirement: there should be visible CMAKE and GCC in PATH. For GCC use setup.sh from /cvmfs/sft.cern.ch/lcg/contrib
# As an argument give the specific version of clang
# The plugins and build options are similar to the root-install-clang-build task in jenkins
# More info on:
# https://llvm.org/docs/GettingStarted.html

# This script successfully builds clang 3.9.0 and 5.0.0 [2017.11.17]

if [ $# -ge 3 ]; then
  VERSION=$1; shift
  GCCCOMPILER=$1; shift
  OS=$1; shift
else
  echo "$0: expecting 3 arguments: [version] [gcccompiler] [OS:centos7/slc6]"
  return
fi

LIBCXX=OFF

source /cvmfs/sft.cern.ch/lcg/contrib/gcc/$GCCCOMPILER/x86_64-$OS/setup.sh
export PATH=/cvmfs/sft.cern.ch/lcg/contrib/CMake/3.8.1/Linux-x86_64/bin:$PATH

wget http://releases.llvm.org/$VERSION/llvm-$VERSION.src.tar.xz
wget http://releases.llvm.org/$VERSION/cfe-$VERSION.src.tar.xz
#wget  http://releases.llvm.org/$VERSION/compiler-rt-$VERSION.src.tar.xz
wget http://releases.llvm.org/$VERSION/clang-tools-extra-$VERSION.src.tar.xz
wget http://releases.llvm.org/$VERSION/openmp-$VERSION.src.tar.xz

tar xf llvm-$VERSION.src.tar.xz
tar xf cfe-$VERSION.src.tar.xz
#tar xf compiler-rf-$VERSION.src.tar.xz
tar xf clang-tools-extra-$VERSION.src.tar.xz
tar xf openmp-$VERSION.src.tar.xz

mv llvm-$VERSION.src llvm
mv cfe-$VERSION.src llvm/tools/clang
#mv compiler-rf-$VERSION.src llvm/projects/compiler-rf
mv clang-tools-extra-$VERSION.src llvm/tools/clang/tools/extra
mv openmp-$VERSION.src llvm/projects/openmp

if [[ "$LIBCXX" == "ON" ]]; then
wget http://releases.llvm.org/$VERSION/libcxx-$VERSION.src.tar.xz
wget http://releases.llvm.org/$VERSION/libcxxabi-$VERSION.src.tar.xz
tar xf libcxx-$VERSION.src.tar.xz
tar xf libcxxabi-$VERSION.src.tar.xz
mv libcxx-$VERSION.src llvm/projects/libcxx
mv libcxxabi-$VERSION.src llvm/projects/libcxxabi
fi

mkdir llvm/build
cd llvm/build
cmake -DCMAKE_BUILD_TYPE=Release -DLLVM_BUILD_LLVM_DYLIB=ON -DGCC_INSTALL_PREFIX=/cvmfs/sft.cern.ch/lcg/contrib/gcc/$GCCCOMPILER/x86_64-$OS/ -DLLVM_TARGETS_TO_BUILD=host -DLLVM_ENABLE_ASSERTIONS=ON -DCMAKE_INSTALL_PREFIX=../../clang-$VERSION -DLLVM_INCLUDE_EXAMPLES=OFF ..

make -j12
make install


