#!/bin/bash

# Create a Tensorflow wheel file to install Tensorflow using pip without root
#
# This script does not rely on the CMake/LCGCMake workflow usually followed by
# the SFT group to install packages.
#
# At the time of writing this script, neither the instructions to install TF
# from source provided by google or using the existing pip packages work with
# our compilers (/cvmfs/sft.cern.ch/lcg/contrib/gcc/).
#
# Therefore, this script downloads and installs all the requirements to generate
# a wheel package to be installed with pip, which is depedendent of the platform
# as well as the compiler used.
#
# As a first step, if we are working with a non-native compiler, we will have
# to run the corresponding setup.sh. Once done, the generated wheel file can
# be used to install tensorflow with pip.
#
# NOTE: Run source /cvmfs/sft.cern.ch/lcg/contrib/gcc/<version>/<platform>/setup.sh
# manually instead of using jk-setup.sh. Somehow, one of the set environment variables
# produces a wrong bazel installation. 
#
# Script based on the following script;
# https://gist.github.com/taylorpaul/3e4e405ffad1be8355bf742fa24b41f0


THIS=$(readlink -f $(dirname ${BASH_SOURCE[0]}))
ARCH=$(uname -m)

PATCHDIR=$THIS/patch

testCC=`which gcc`
os=`cat /etc/lsb-release | head -1`

#VERSIONS
PYTHON_VERSION=3.6.5
SQLITE_VERSION=3110100
SETUPTOOLS_VERSION=39.2.0

STARTDIR=`pwd`/tensorflow_tmp
GCC_DIR=$LCG_gcc_home
BAZEL_BIN_DIR=$STARTDIR/bin #/bin where to copy bazel binary
PYTHON_INSTALL_DIR=$STARTDIR/python
LOCAL_INCLUDE=$STARTDIR/include
LOCAL_LIBRARY=$STARTDIR/lib
DOWNLOADS_DIR=$STARTDIR/downloads
PIP_DIR=$LOCAL_LIBRARY/python$PYTHON_VERSION
export PYTHONPATH=$PIP_DIR/site-packages:$PYTHONPATH
#Bazal settings
export TEST_TMPDIR=$STARTDIR/bazal_tmp


echo "Building Directories"
for d in $BAZEL_BIN_DIR $PYTHON_INSTALL_DIR  $LOCAL_INCLUDE $LOCAL_LIBRARY $DOWNLOADS_DIR; do
      mkdir -p $d
done

# Get python command
python_two_digits_ver=`echo $PYTHON_VERSION | cut -d"." -f -2`
PYTHON_CMD=python$python_two_digits_ver

#DOWNLOADS
cd $DOWNLOADS_DIR
(test -e Python-$PYTHON_VERSION.tgz && echo "Python-$PYTHON_VERSION.tgz already exists") || \
(wget https://www.python.org/ftp/python/$PYTHON_VERSION/Python-$PYTHON_VERSION.tgz 2>/dev/null && echo "Python downloaded")

(test -e setuptools-$SETUPTOOLS_VERSION.tar.gz && echo "setuptools-$SETUPTOOLS_VERSION.tgz already exists") || \
(wget --no-check-certificate https://files.pythonhosted.org/packages/1a/04/d6f1159feaccdfc508517dba1929eb93a2854de729fa68da9d5c6b48fa00/setuptools-39.2.0.zip -O setuptools-$SETUPTOOLS_VERSION.zip 2>/dev/null && echo "setuptools-$SETUPTOOLS_VERSION.tgz downloaded")
# Oracle require accepting their license before you can get the JRE, solved passing a cookie

(test -e sqlite-autoconf-$SQLITE_VERSION.tar.gz && echo "sqlite-autoconf-$SQLITE_VERSION.tar.gz already exists") || \
    (wget https://sqlite.org/2016/sqlite-autoconf-$SQLITE_VERSION.tar.gz 2>/dev/null && echo "sqlite-autoconf-$SQLITE_VERSION.tar.gz downloaded")
#TODO: update if newer version needed (3.8.6)

#Unzip archives
echo "Decompressing archives"
tar zxf $DOWNLOADS_DIR/Python-$PYTHON_VERSION.tgz && echo "Python $PYTHON_VERSION decompressed"
unzip $DOWNLOADS_DIR/setuptools-$SETUPTOOLS_VERSION.zip && echo "setuptools $SETUPTOOLS_VERSION decompressed"
tar --totals -xf $DOWNLOADS_DIR/sqlite-autoconf-$SQLITE_VERSION.tar.gz && echo "sqlite $SQLITE_VERSION decompressed"

cd sqlite-autoconf-$SQLITE_VERSION
SQLITE_INSTALL_DIR=`pwd`
if [ -e $LOCAL_INCLUDE/sqlite3.h ] && [ -e $LOCAL_LIBRARY/libsqlite3.a ]; then
  echo "sqlite already installed"
else
  echo "Installing sqlite3 libs at `pwd`!"
  ./configure --enable-shared --prefix=$SQLITE_INSTALL_DIR
  make
  make install
  cp ./include/* $LOCAL_INCLUDE
  cp ./lib/* $LOCAL_LIBRARY
fi
cd .. # Go back to $DOWNLOADS

cd Python-$PYTHON_VERSION
if [ -e $PYTHON_INSTALL_DIR/bin/$PYTHON_CMD ];then
  echo "Python already installed"
else
  echo "Installing python at $PYTHON_INSTALL_DIR"
  sed -i 's#/usr/local/include/sqlite3#'$LOCAL_INCLUDE'#g' ./setup.py
  ./configure --enable-shared --prefix=$PYTHON_INSTALL_DIR --enable-unicode=ucs4
  make
  make altinstall
  export PATH=$PYTHON_INSTALL_DIR/bin:$PATH
fi
cd .. # Go back to $DOWNLOADS

export PATH=$PYTHON_INSTALL_DIR/bin:$PATH
export LD_LIBRARY_PATH=$PYTHON_INSTALL_DIR/lib:$LD_LIBRARY_PATH


if [ -e $PYTHON_INSTALL_DIR/bin/pip ]; then
  echo "Pip already installed"
else
  echo "----- Installing Pip"
  cd setuptools-$SETUPTOOLS_VERSION
  $PYTHON_INSTALL_DIR/bin/$PYTHON_CMD setup.py install
  (curl https://bootstrap.pypa.io/get-pip.py | $PYTHON_INSTALL_DIR/bin/$PYTHON_CMD -) && echo "Pip installed"
  pip install --no-cache-dir numpy
  pip install -U nltk
  cd ..
fi

pip install enum34 mock virtualenv


if [[ $testCC == */usr/bin/gcc* ]] && [[ $os != *Ubuntu* ]]; then
  echo "Please set the compiler from CVMFS"
  echo "Example:"
  echo "$ source /cvmfs/sft.cern.ch/lcg/contrib/gcc/<version>/<platform>/setup.sh"
  return 1
fi

# Get python command
PYTHON_VERSION=$(echo $($PYTHON_CMD --version 2>&1) | cut -f2 -d' ' | cut -c-3)


echo "Compiler $GCC_DIR"
which gcc
which $PYTHON_CMD
echo PYTHONPATH=$PYTHONPATH
which java
C_INCLUDE_PATH=$STARTDIR/include
mkdir -p $STARTDIR/include
touch $STARTDIR/include/stropts.h # STREAMS not supported by linux - make empty file to let it compile
                                  # http://www.lampdocs.com/stropts-h-no-such-file-or-directory-how-to-fix/

#VERSIONS
BAZEL_VERSION=0.13.0
TF_VERSION=1.8.0
TB_VERSION=1.8.0

cd $DOWNLOADS_DIR
echo "Compiling bazel in `pwd`/bazel"
if [ -e bazel ]; then
   echo "bazel already downloaded"
else
   wget https://github.com/bazelbuild/bazel/releases/download/$BAZEL_VERSION/bazel-$BAZEL_VERSION-dist.zip
   unzip bazel-$BAZEL_VERSION-dist.zip -d bazel 1> /dev/null
fi
cd bazel

if [ -e $BAZEL_BIN_DIR/bazel ]; then
    echo "Bazel already installed"
else
    #patch src/main/java/com/google/devtools/build/lib/analysis/skylark/SkylarkActionFactory.java $THIS/patch/bazel.patch
    #Don't clean the environment in bazel
    patch src/main/java/com/google/devtools/build/lib/shell/JavaSubprocessFactory.java $THIS/patch/bazel.patch
    # Need to modify max number of threads
    #ulimit -u 100000
    ./compile.sh
    cp ./output/bazel $BAZEL_BIN_DIR/bazel
fi
cd ..

export PATH=$BAZEL_BIN_DIR:$PATH

echo "Compiling tensorflow in `pwd`/tensorflow"
if [ ! -e tensorflow-${TF_VERSION} ]; then
    wget https://github.com/tensorflow/tensorflow/archive/v${TF_VERSION}.tar.gz -O tensorflow-${TF_VERSION}.tar.gz
    tar -xf tensorflow-${TF_VERSION}.tar.gz
fi

####################
##BUILD TENSORFLOW##
####################
cd tensorflow-${TF_VERSION}
TF_INSTALL_DIR=`pwd`
export PYTHON_BIN_PATH=${PYTHON_INSTALL_DIR}/bin/$PYTHON_CMD 
export PYTHON_INCLUDE_PATH=${PYTHON_INSTALL_DIR}/include/python$PYTHON_VERSION
export PYTHON_LIB_PATH=${PYTHON_INSTALL_DIR}/lib
export USE_DEFAULT_PYTHON_LIB_PATH=1


if ! ls "$TF_INSTALL_DIR/tensorflow_pkg" | grep "^tensorflow.*\.whl" ; then
# Preparing environment variables for tensorflow configure
export CC_OPT_FLAGS="-march=native -mno-bmi2"
export TF_NEED_JEMALLOC=0
export TF_NEED_GCP=0
export TF_NEED_HDFS=0
export TF_ENABLE_XLA=0
export TF_NEED_OPENCL=0
export TF_NEED_CUDA=0
export TF_NEED_KAFKA=0
export TF_NEED_OPENCL_SYCL=0
export TF_NEED_GCP=0
export TF_NEED_HDFS=0
export TF_NEED_S3=0
export TF_NEED_GDR=0
export TF_NEED_VERBS=0
export TF_NEED_OPENCL=0
export TF_NEED_MPI=0
export TF_NEED_TENSORRT=0
export TF_SET_ANDROID_WORKSPACE=0
export TF_DOWNLOAD_CLANG=0
export GCC_HOST_COMPILER_PATH=${CC}

cp configure configure.orig
sed 's/bazel clean --expunge/bazel clean --expunge_async/g' configure.orig > configure

sed -i "s/std=c++0x/std=c++11/g" tensorflow_tmp/downloads/tensorflow-1.8.0/third_party/toolchains/cpus/arm/CROSSTOOL.tpl
#Bazel patched - the whole env is always passed
#patch tensorflow/tensorflow.bzl $PATCHDIR/tensorflow.patch 
patch tensorflow/tensorflow.bzl $PATCHDIR/tensorflow.bzl.patch

#Tensorflow 1.5.0 patch
#patch WORKSPACE $PATCHDIR/workspace.patch
#sed -i '/check_version("0.5.4")/d' tensorflow/workspace.bzl

#Modify tensorflow CROSSTOOL.tpl file:
cp $TF_INSTALL_DIR/third_party/gpus/crosstool/CROSSTOOL_clang.tpl $TF_INSTALL_DIR/third_party/gpus/crosstool/CROSSTOOL_clang_ORIG.tpl
cp $TF_INSTALL_DIR/third_party/gpus/crosstool/CROSSTOOL_nvcc.tpl $TF_INSTALL_DIR/third_party/gpus/crosstool/CROSSTOOL_nvcc_ORIG.tpl
sed -i 's#/usr/bin#'$GCC_DIR'/bin#g' $TF_INSTALL_DIR/third_party/gpus/crosstool/CROSSTOOL_clang.tpl
sed -i 's#/usr/bin#'$GCC_DIR'/bin#g' $TF_INSTALL_DIR/third_party/gpus/crosstool/CROSSTOOL_nvcc.tpl

#Modify tensorflow crosstool_wrapper_driver_is_not_gcc.tpl file
cp $TF_INSTALL_DIR/third_party/gpus/crosstool/clang/bin/crosstool_wrapper_driver_is_not_gcc.tpl \
$TF_INSTALL_DIR/third_party/gpus/crosstool/clang/bin/crosstool_wrapper_driver_is_not_gcc_ORIG.tpl
sed -i 's#/usr/bin/gcc/#'$GCC_DIR'/bin/gcc#g' \
$TF_INSTALL_DIR/third_party/gpus/crosstool/clang/bin/crosstool_wrapper_driver_is_not_gcc.tpl

# Solve the import error undefined symbol: clock_gettime
#sed -i 's/return \[\]  # No extension link opts/return ["-lrt"]/g' $TF_INSTALL_DIR/tensorflow/tensorflow.bzl

$TF_INSTALL_DIR/configure
echo "build:opt --host_copt=-mno-bmi2" >> $TF_INSTALL_DIR/.tf_configure.bazelrc

#Can't just use the basic call from tensorflow.org install directions:
echo "BUILD TENSORFLOW //tensorflow/tools/pip_package:build_pip_package"
$BAZEL_BIN_DIR/bazel build --verbose_failures --linkopt='-lrt' --config=cvmfs --host_copt="-mno-bmi2" --copt="-mno-bmi2" --host_cxxopt="-lrt" --host_copt="-lrt" --copt="-lrt" --cxxopt="-lrt" //tensorflow/tools/pip_package:build_pip_package //tensorflow:libtensorflow.so //tensorflow:libtensorflow_cc.so
bazel-bin/tensorflow/tools/pip_package/build_pip_package $TF_INSTALL_DIR/tensorflow_pkg
fi
cd ..
#

#####################
##BUILD TENSORBOARD##
#####################

TB_ARCHIVE=tensorboard-$TB_VERSION.tar.gz
TB_INSTALL_DIR=tensorboard-$TB_VERSION

if [ ! -e $TB_ARCHIVE ]; then
    wget -O $TB_ARCHIVE  https://github.com/tensorflow/tensorboard/archive/${TB_VERSION}.tar.gz
fi

if [ ! -e $TB_INSTALL_DIR ]; then
    tar -xf $TB_ARCHIVE
fi

cd $TB_INSTALL_DIR
if [ ! -z "$(ls -1)" ]; then
    #Patches for tensorboard 1.5.1
    #patch WORKSPACE $PATCHDIR/workspace_tb.patch
    #sed -i '/check_version("0.5.4")/d' tensorboard/workspace.bzl
    #patch -p0 -i $PATCHDIR/upgrade_protobuf_closure.patch

    #$BAZEL_BIN_DIR/bazel fetch //...
    #patch $($BAZEL_BIN_DIR/bazel info output_base)/external/protobuf/protobuf.bzl $THIS/patch/protobuf.patch
    $BAZEL_BIN_DIR/bazel build  --verbose_failures --config=opt //tensorboard/pip_package:build_pip_package

	if [[ "$PYTHON_CMD" != "python3" ]]; then
	    patch tensorboard/pip_package/build_pip_package.sh $THIS/patch/python.patch
	fi
	
	bazel-bin/tensorboard/pip_package/build_pip_package $TF_INSTALL_DIR/tensorflow_pkg
	
fi

cd ..



#Get name of the created whl file:
for filename in $TMPDIR/tensorflow_pkg/*;
do
    export TF_WHEEL_FILE=$filename
done

if [ -e $TF_WHEEL_FILE ]; then
    echo "Tensorflow wheel file created for platform: $PLATFORM"
    echo "Path: $TF_INSTALL_DIR/tensorflow_pkg/$TF_WHEEL_FILE"
fi

# Pack header files and shared libraries
PKG=$TF_INSTALL_DIR/tensorflow_pkg/tensorflow
mkdir -p $PKG/include/tensorflow_externals
mkdir -p $PKG/lib

cd $TF_INSTALL_DIR/bazel-tensorflow-${TF_VERSION}
find -L tensorflow -iname "*.h" -exec cp --parents {} $PKG/include \;

cp $TF_INSTALL_DIR/bazel-bin/tensorflow/libtensorflow{,_cc,_framework}.so $PKG/lib

cd $TF_INSTALL_DIR/bazel-tensorflow-${TF_VERSION}/external
find -L nsync -iname "*.h" -exec cp --parents {} $PKG/include/tensorflow_externals \;

cd $PKG/include
find . -iname "*.h" -exec sed -i "s:\"\(third_party/.*\)\":<\1>:" {} \;

cd $PKG
tar czvf tensorflow-$TF_VERSION.tar.gz *


