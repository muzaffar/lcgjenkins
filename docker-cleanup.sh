#!/bin/bash -x

# Script to clean up docker containers.

# Beware the code here highly relies on multiple variable defined inside the
# jenkins job configuration

NAME=`echo $BUILD_TAG | tr "," "-" | tr "=" "-"`

if [ "$(docker ps -q -f status=exited  -f name=$NAME)" ]; then
    # cleanup
    docker rm $NAME
fi
