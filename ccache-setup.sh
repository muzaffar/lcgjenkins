#!/bin/bash -x

export CCACHE_PATH=$PATH
export PATH=/usr/local/bin:$PATH
export CC=`which gcc`
export CXX=`which g++`
export FC=`which gfortran`
mkdir -p ~/.ccache
cp -r /ccache/ccache.conf ~/.ccache/ccache.conf
