# -*- coding: utf-8 -*-

"""
releasepy.descriptor
~~~~~~~~~~~~~~~
This module parse a release description file and give all its information
"""

import os
import paths
import utils
import subprocess
import package
import buildinfo2json

class Descriptor(object):
    """Description of the package
    :param sourcefile: Data source for all the information about packages in a release
    """
    def __init__(self, filename, endsystem, platform=None, version=None):
        self.filename = filename
        if platform:
            self.platform = platform
        else:
            self.platform = utils.getPlatformFromFilename(self.filename)
        if version:
            self.version = version
        else:
            self.version = utils.getVersionFromFilename(self.filename)
        self.content = self.getDescriptionContent()
        self.lines = self.content.split('\n')

        # Created in lazy mode
        self.PKGS_OK = None
        self.allpackages = None

    def getDescriptionURL(self):
        releaseURL = paths.release_readonly(self.endsystem)
        return os.path.join(releaseURL, self.filename)

    def getDescriptionContent(self):
        url = self.getDescriptionURL()
        if not utils.checkURL(url):
          raise RuntimeError("URL {0} not found.".format(url))
        p = subprocess.Popen(['curl', '-s', url], stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        stdout, stderr = p.communicate()
        if p.returncode == 0:
          return stdout.strip()
        else:
          print "ERROR:"
          print stderr.strip()
          raise RuntimeError("Cannot get info from " + url)

    def isLimited(self):
        return self.lines[0].startswith('#') and self.lines[1].startswith('#')

    def __getPKGS_OK_names(self):
        if not self.isLimited():
            self.PKGS_OK = {}
        else:
            firstline = self.lines[0:1]
            for i in firstline:
                selectedpackages = i.split('PKGS_OK ')[1]
            lines = self.lines[2:]  # drop comment
            selectedpackages = selectedpackages.split(' ')
            for line in lines:
                d = buildinfo2json.parse(line)
                for i in selectedpackages:
                    if i != d['NAME']:
                        continue
                    else:
                        if d['NAME'] != d['DESTINATION']:
                            print "# Skip package", d['NAME'], 'as it should be packaged in', d['DESTINATION']
                            continue
                        p = package.ExtendedPackage(d['NAME'], d['VERSION'], d['HASH'], d['DIRECTORY'], d['DEPENDS'], self.platform, d['COMPILER'])
                        self.PKGS_OK[p.getPackageName()] = p

    def getPKGS_OK_names(self):
        if self.PKGS_OK is None:
            self.__getPKGS_OK()
        return list(self.PKGS_OK.keys())

    def getPKGS_OK(self):
        if self.PKGS_OK is None:
            self.__getPKGS_OK()
        return list(self.PKGS_OK.values())

    def __getAllPackages(self):
        if self.isLimited:
            lines = self.lines[2:]  # drop comment
        else:
            lines = self.lines[1:]  # drop comment

        for line in lines:
            d = buildinfo2json.parse(line)
            if d['NAME'] != d['DESTINATION']:
                # bundled package
                print "# Skip package", d['NAME'], 'as it should be packaged in', d['DESTINATION']
                continue
            p = package.ExtendedPackage(d['NAME'], d['VERSION'], d['HASH'], d['DIRECTORY'], d['DEPENDS'], self.platform, d['COMPILER'])
            self.allpackages[p.getPackageName()] = p

    def getAllPackagesNames(self):
        if self.allpackages_names is None:
            self.__getAllPackages()
        return list(self.allpackages.keys())

    def getAllPackages(self):
        if self.allpackages is None:
            self.__getAllPackages()
        return list(self.allpackages.values())

    def getPackages(self, pkglist):
        subgrp_packages = []
        for pkg in pkglist:
            subgrp_packages.append(self.getAllPackages.get(pkg))
        return subgrp_packages
