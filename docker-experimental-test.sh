export VIEW=`head -1 $WORKSPACE/lcgjenkins/PARAM`
tail -2 $WORKSPACE/lcgjenkins/PARAM > $WORKSPACE/lcgtest/TAG

export WORKSPACE_HOST=$WORKSPACE
export WORKSPACE='/build/jenkins/workspace'

# Extract name from build tag
export NAME=`echo $BUILD_TAG | tr "," "-" | tr "=" "-"`

# Prepare number of cpus to use in the container
TOTALCPU=`nproc --all`
CONTAINERS_LIMIT=$(($EXECUTOR_NUMBER+1))

if [ -z $DOCKER_CPUS ]; then
    DOCKER_CPUS=$(($TOTALCPU/$CONTAINERS_LIMIT))
fi

USER_ID=$(id $(whoami) -u)
GROUP_ID=$(id $(whoami) -g)


if   [[ $SLOTNAME == dev* ]]; then export MODE=$SLOTNAME
elif [[ $SLOTNAME == exp* ]]; then export MODE=Experimental
elif [ -z $SLOTNAME ];        then export MODE=Experimental
else                               export MODE=Release; fi

# Run container and full build inside
docker exec -e WORKSPACE=$WORKSPACE \
            -e LCG_VERSION=$LCG_VERSION \
            -e LCG_IGNORE="$LCG_IGNORE" \
            -e BUILDMODE=$BUILDMODE \
            -e LCG_INSTALL_PREFIX=$LCG_INSTALL_PREFIX \
            -e LCG_EXTRA_OPTIONS=$LCG_EXTRA_OPTIONS   \
            -e TARGET=$TARGET       \
            -e TEST_LABELS=$TEST_LABELS \
            -e USER=sftnight            \
            -e SHELL=$SHELL             \
            -e GIT_COMMIT=$GIT_COMMIT   \
            -e PROPERTIES_PATH=/lcgjenkins \
            -e BUILDHOSTNAME=$HOSTNAME-docker \
            -e MODE=$MODE \
                $NAME \
                bash -c "/lcgjenkins/runtest-docker.sh $PATTERN $VIEW "

