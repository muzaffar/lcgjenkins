#!/bin/bash -x
if [ $# -ge 1 ]; then
  PLATFORM=$1; shift
else
  echo "$0: expecting 1 arguments: [platform]"
  return
fi

weekday=`date +%a`
export PLATFORM
export WORKDIR=$WORKSPACE
export weekday
export ENDSYSTEM=$BACKEND

if [[ $ENDSYSTEM == *cvmfs* ]]; then
  # Starting the cvmfs commands to ensure a transaction
  echo "Installing on CVMFS"
  $WORKDIR/lcgjenkins/cvmfs_install.sh
else
  # Installation to afs
  echo "Installing on AFS"
  $WORKDIR/lcgjenkins/afs_install.sh
fi
