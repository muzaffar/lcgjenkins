#!/bin/bash -x

# This script is intended to copy results of builds to the Master node

if [ $# -ge 1 ]; then
  SLOTNAME=$1; shift
else
  echo "$0: expecting 1 arguments: [lcg_version]"
  return
fi

export SLOTNAME
weekday=`date +%a`
SCP="scp -B"
CopyStatus=0

if [[ $PLATFORM == *mac* ]]; then
    kinit -V -k -t /build/conf/sftnight.keytab
else
    kinit sftnight@CERN.CH -5 -V -k -t /ec/conf/sftnight.keytab
fi

if [ "${BUILDMODE}" == "nightly" ]; then
    masterspace=/build/workspace/nightlies-tarfiles/$SLOTNAME/$weekday/
else
    masterspace=/build/workspace/releases-tarfiles/
fi

cd $WORKSPACE/build

if [ "${BUILDMODE}" == "nightly" ]; then
    $SCP isDone* sftnight@epsft-jenkins.cern.ch:$masterspace
    CopyStatus=$?
fi
scp *.txt sftnight@epsft-jenkins.cern.ch:$masterspace
ret=$?
CopyStatus=`(test $ret -ne 0 && echo $ret) || echo $CopyStatus`

if [ "$(ls -A tarfiles)" ]; then
    echo "Take action: tarfiles is not Empty"
    cd tarfiles
    $SCP *.tgz sftnight@epsft-jenkins.cern.ch:$masterspace
    ret=$?
    CopyStatus=`(test $ret -ne 0 && echo $ret) || echo $CopyStatus`
else
    echo "tarfiles directory is empty. I will assume this is not an error though"
    exit 0
fi

exit $CopyStatus

