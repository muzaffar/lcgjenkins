#!/usr/bin/env python
import sys, glob, os
import Tools as tools
import shutil

# Removal of nightly builds from CVMFS or AFS

##########################
if __name__ == "__main__":
    from optparse import OptionParser
    parser = OptionParser()

    (options, args) = parser.parse_args()
    if len(args)!=4:
        print "Please provide a slot name and platform and day of the week and backend to clean!"
        sys.exit(-2)
    slot = args[0]
    platform = args[1]
    day_option = args[2]
    endsystem=args[3]

    if day_option=="tomorrow":
        day = tools.tomorrow()    
    else:
        day = day_option 


    if "afs" in endsystem:
        BASE="/afs/cern.ch/sw/lcg/app/nightlies"
    else:
        BASE="/cvmfs/sft-nightlies.cern.ch/lcg/nightlies"

    print "Cleaning nightly builds of slot '%s' for day %s" %(slot,day) 

    rootdir = tools.pathJoin(BASE,slot,day)
    dirs = glob.glob(os.path.join(rootdir, '*/*', platform))
    dirs.extend(glob.glob(os.path.join(rootdir, '*/*/*', platform)))
    dirs.extend(glob.glob(os.path.join(rootdir, '*/*/*/*', platform)))

    for d in dirs:
      if os.path.islink(d):
          print 'Unlinking directory %s' % (d)
          os.unlink(d)
      else:
          print 'Removing  directory %s' % (d)
          shutil.rmtree(d)
      #---prune empty directories
      p = os.path.dirname(d)
      while p != rootdir :
         if not os.listdir(p):
            print 'Pruning empty directory %s' % (p)
            os.rmdir(p)
         p = os.path.dirname(p)
