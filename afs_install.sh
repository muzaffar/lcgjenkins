#!/bin/bash
echo "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX"
echo LCG_${LCG_VERSION}_$PLATFORM.txt
echo "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX"

if [[ $PLATFORM == *slc6* || $PLATFORM == *cc7* || $PLATFORM == *centos7* || $PLATFORM == *ubuntu* ]]; then
    kinit sftnight@CERN.CH -5 -V -k -t /ec/conf/sftnight.keytab
fi

weekday=`date +%a`


basewrite="/afs/.cern.ch/sw/lcg/releases"
baseread="/afs/cern.ch/sw/lcg/releases"

installbasenightly="/afs/cern.ch/sw/lcg/app/nightlies"

array=(${PLATFORM//-/ })
comp=`echo ${array[2]}`

abort=0

echo "This is the value of the BUILDMODE: ---> ${BUILDMODE}"

if [ "${BUILDMODE}" == "nightly" ]; then
    export NIGHTLY_MODE=1
    $WORKSPACE/lcgjenkins/clean_nightlies.py ${LCG_VERSION} $PLATFORM $weekday afs
    rm $installbasenightly/${LCG_VERSION}/$weekday/isDone-$PLATFORM
    rm $installbasenightly/${LCG_VERSION}/$weekday/isDone-unstable-$PLATFORM
    $WORKSPACE/lcgjenkins/lcginstall.py -y -u http://lcgpackages.web.cern.ch/lcgpackages/tarFiles/nightlies/${LCG_VERSION}/$weekday -r ${LCG_VERSION} -d LCG_${LCG_VERSION}_$PLATFORM.txt -p $installbasenightly/${LCG_VERSION}/$weekday/ -e afs
    if [ "\$?" == "0" ]; then
	echo "Installation script has worked, we go on"
    else
	echo "there is an error installing the packages. Let's give it a chance though ..."
    fi

    cd  $installbasenightly/${LCG_VERSION}/$weekday/
    wget https://lcgpackages.web.cern.ch/lcgpackages/tarFiles/nightlies/${LCG_VERSION}/$weekday/isDone-$PLATFORM
    wget https://lcgpackages.web.cern.ch/lcgpackages/tarFiles/nightlies/${LCG_VERSION}/$weekday/isDone-unstable-$PLATFORM
    if [ -f "$installbasenightly/${LCG_VERSION}/$weekday/isDone-$PLATFORM" ]; then
	echo "The installation of the nightly is completed with all packages"
	$WORKSPACE/lcgjenkins/extract_LCG_summary.py $installbasenightly/${LCG_VERSION}/$weekday $PLATFORM ${LCG_VERSION} RELEASE
	$WORKSPACE/lcgcmake/cmake/scripts/create_lcg_view.py -l $installbasenightly/${LCG_VERSION}/$weekday -p $PLATFORM -d -B /afs/cern.ch/sw/lcg/views/${LCG_VERSION}/$weekday/$PLATFORM
#        afs_admin vos_release p.sw.lcg
        exit 0
    elif [ -f "$installbasenightly/${LCG_VERSION}/$weekday/isDone-unstable-$PLATFORM" ]; then
	$WORKSPACE/lcgjenkins/extract_LCG_summary.py $installbasenightly/${LCG_VERSION}/$weekday $PLATFORM ${LCG_VERSION} RELEASE
        echo "The creation of the views has not worked. We exit here"
        exit 1
    fi

elif [ "${BUILDMODE}" == "release" ]; then
   $WORKSPACE/lcgjenkins/lcginstall.py -u http://lcgpackages.web.cern.ch/lcgpackages/tarFiles/releases -r ${LCG_VERSION} -d LCG_${LCG_VERSION}_$PLATFORM.txt -p $basewrite -e afs

   cd $basewrite/LCG_${LCG_VERSION}
   $WORKSPACE/lcgjenkins/extract_LCG_summary.py . $PLATFORM ${LCG_VERSION} RELEASE
   afs_admin vos_release p.sw.lcg.releases
   afs_admin vos_release p.sw.lcg.relmcgen
   afs_admin vos_release p.sw.lcg.relroot
   $WORKSPACE/lcgcmake/cmake/scripts/create_lcg_view.py -l $baseread -p $PLATFORM -r ${LCG_VERSION} -d -B /afs/.cern.ch/sw/lcg/views/LCG_${LCG_VERSION}/$PLATFORM
#   if [ "\$?" == "0" ]; then
#      echo "Views creation script has worked, we finish"
      afs_admin vos_release p.sw.lcg
#    else
#      echo "there is an error creating views. Exiting ..."
#      exit 1
#    fi


elif [ "${BUILDMODE}" == "limited" ]; then
    if [[ "${UPDATELINKS}" == "false" ]]; then
        $WORKSPACE/lcgjenkins/lcginstall.py -o -u http://lcgpackages.web.cern.ch/lcgpackages/tarFiles/releases -r ${LCG_VERSION} -d LCG_${LCG_VERSION}_$PLATFORM.txt -p $basewrite -e afs
    else
        $WORKSPACE/lcgjenkins/lcginstall.py -o -u http://lcgpackages.web.cern.ch/lcgpackages/tarFiles/releases -r ${LCG_VERSION} -d LCG_${LCG_VERSION}_$PLATFORM.txt --update -p $basewrite -e afs
    fi
    afs_admin vos_release p.sw.lcg.releases
    afs_admin vos_release p.sw.lcg.relmcgen
    afs_admin vos_release p.sw.lcg.relroot
    $WORKSPACE/lcgcmake/cmake/scripts/create_lcg_view.py -l $baseread/LCG_${LCG_VERSION} -p $PLATFORM -d -B /afs/.cern.ch/sw/lcg/views/LCG_${LCG_VERSION}/$PLATFORM
    afs_admin vos_release p.sw.lcg


fi
