#!/bin/bash

# Accepting following parameters:
#  $1: build platform
#  $2: architecture (avx, avx2, sse2...)

platform=$1; shift

# Extract second last item from platform (compiler)
compiler=`echo $platform | awk -F '-' '{print $(NF-1)}'`
echo $platform

# Get compiler version number
comp_vers=`echo $compiler | tr -d -c 0-9`

# Consider gcc7 as gcc70 for later comparision
if [ $comp_vers -eq 7 ]; then
  comp_vers=70
fi

if [ $# -ge 1 ]; then
  ARCHITECTURE=$1; shift
  echo "Using compiler wrapper for: $compiler with $ARCHITECTURE"

  unset arch_params

IFS='+ ' read -r -a array <<EOF
$ARCHITECTURE
EOF

  for element in ${array[@]}
  do
      arch+=m$element+
      arch_params+="-m$element "
  done

  CXXOPTIONS=${arch%?}
  WRAPPER_CXX_FLAGS=${arch_params}
  WRAPPER_CC_FLAGS=${arch_params}

  #### Instruction set
  #export LCG_EXT_ARCH="-march=core-${ARCHITECTURE}"
  export ARCHITECTURE
  export CXXOPTIONS
  export WRAPPER_CXX_FLAGS
  export WRAPPER_CC_FLAGS

else
  echo "Using compiler wrapper for: $compiler without any flags"
fi

# Reset the variable
unset LCG_CXX_FLAGS

export LCG_CPP11="FALSE"
export LCG_CPP1Y="FALSE"
export LCG_CPP14="FALSE"
export LCG_CPP17="FALSE"

#handling of C++ flags
# enable it only for gcc 4.6 and greater
function cxx11_flag {
  if [[ $compiler  == *gcc* ]]; then
    if [ $comp_vers -gt 46 ]; then
      export LCG_CXX_FLAGS="-std=c++11"
      export LCG_CPP11="TRUE"
      return 0
    fi
  elif [[ $compiler == *clang* ]]; then
    export LCG_CXX_FLAGS="-std=c++11"
    export LCG_CPP11="TRUE"
    return 0
  fi
  return 1
}

#handling of C++1y
# enable it only for gcc 4.9 and greater
function cxx1y_flag {
  if [[ $compiler  == *gcc* ]]; then
    if [ $comp_vers -gt 48 ]; then
      export LCG_CXX_FLAGS="-std=c++1y"
      export LCG_CPP1Y="TRUE"
      return 0
    fi
  fi
  return 1
}

#handling of C++14
# enable it only for gcc 5.1 and greater
function cxx14_flag {
  if [[ $compiler  == *gcc* ]]; then
    if [ $comp_vers -gt 50 ]; then
      export LCG_CXX_FLAGS="-std=c++14"
      export LCG_CPP14="TRUE"
      return 0
    fi
  elif [[ $compiler == *clang* ]]; then
    export LCG_CXX_FLAGS="-std=c++14"
    export LCG_CPP14="TRUE"
    return 0
  fi
  return 1
}

# handling of C++17                                                                                                                                                         
# enable it only for gcc 7.3 and greater
                                                                                                                                    
function cxx17_flag {
  if [[ $compiler  == *gcc* ]]; then
    if [ $comp_vers -gt 72 ]; then
      export LCG_CXX_FLAGS="-std=c++17"
      export LCG_CPP17="TRUE"
      return 0
    fi
  fi
  return 1
}


# Export LCG_CXX_FLAGS
cxx17_flag || cxx14_flag || cxx1y_flag || cxx11_flag

# Call wrapper setup from cvmfs
source /cvmfs/sft.cern.ch/lcg/contrib/gcc/wrapper/setup.sh
