#!/bin/bash
if [ $# -ge 2 ]; then
  BUILDTYPE=$1; shift
  COMPILER=$1; shift
else
  echo "$0: expecting 2 arguments: [buildtype] [compiler]"
  return
fi

export BUILDTYPE
export COMPILER

export PLATFORM=`$WORKSPACE/lcgjenkins/getPlatform.py`

cd /eos/project/l/lcg/www/lcgpackages/tarFiles/releases
echo $PLATFORM

ls -1 | grep $PLATFORM > summary-$PLATFORM.txt
