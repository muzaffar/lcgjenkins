#!/bin/bash -x

# Script to run fully export docker containers after running LCG builds inside.
# The whole container gets stored in EOS with all the info to be restored in
# any other docker host node.

# Beware the code here highly relies on multiple variable defined inside the
# jenkins job configuration

# Job name
export JOB=`echo ${JOB_NAME}-BUILDNUM=${BUILD_NUMBER} | tr "/," "-"`
export NAME=`echo $BUILD_TAG | tr "," "-" | tr "=" "-"`

# Export to cvmfs-server
#docker commit $NAME $NAME

FOLDER=$JOB
IMAGE=image.tar
mkdir $FOLDER

# Export container
time docker export $NAME > ${FOLDER}/${IMAGE}

# Store command to reproduce same environment
echo 'docker run -dit -e WORKSPACE=$WORKSPACE \
            -u sftnight		\
            -v /ccache:/ccache		\
            -v /ec/conf:/ec/conf 	\
            -v /cvmfs:/cvmfs      \
            	IMAGE 	\
              bash \
' > ${FOLDER}/docker_run.sh

# Store it in EOS
kinit sftnight@CERN.CH -5 -V -k -t /ec/conf/sftnight.keytab
export  EOS_MGM_URL=root://eosuser.cern.ch
# Ensure eos is mounted
eosfusebind
eos mkdir -p /eos/project/l/lcg/www/lcgpackages/dockerbuilds/${FOLDER}
time xrdcp -fr $FOLDER root://eosuser.cern.ch//eos/project/l/lcg/www/lcgpackages/dockerbuilds/${FOLDER}

# Clean up local directory
rm -rf $FOLDER

# Kill container
docker rm $NAME

# Clean up files older than 2 days in eos
find /eos/project/l/lcg/www/lcgpackages/dockerbuilds/ -maxdepth 1 -mmin +$((60*24*3)) | xargs rm -rf # files modified more than 3 days ago

# Echo how to use info
echo "Use the following command to restore this build in any docker host:"
echo -e "\n\t/docker_scripts/restore_build.sh ${JOB}\n"
