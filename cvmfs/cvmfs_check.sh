#!/bin/bash

# wait a while to let the system to fully re-boot
sleep 30

# add more cvmfs repostiories if you need below _without_ the starting /cvmfs

cvmfs_repos="sft.cern.ch,sft-nightlies.cern.ch"

for repos in `echo ${cvmfs_repos} |tr "," "\n"`; do
   [ ! -d /cvmfs/${repos} ] && mkdir -p /cvmfs/${repos}
   if /sbin/mount | grep /cvmfs/${repos} > /dev/null; then
      echo "/cvmfs/${repos} already mounted"
   else
      echo "Mounting /cvmfs/${repos}"
      /sbin/mount -t cvmfs ${repos} /cvmfs/${repos}
   fi
done

